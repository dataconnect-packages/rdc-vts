# RDC VTS-XML

Build in PHP for, retrieving car information from RDC VTS-XML based on version 3.0<br>
To retrieve data, you need to be a registered company @ RDC and have access to VTS-XML service.<br>
Sidenote: this is only cardata for registered licenseplates in the Netherlands

## Requirements
vts-xml can be run with PHP 8.0 and above. Not tested with earlier versions.

## Installation
Can be installed with [Composer](https://getcomposer.org/).

To get the latest version of rdw-ovi use:
```bash
composer require dataconnect/rdc-vts
```

## Example
```bash
use RdcVts\RdcVtsApiClient;
use RdcVts\RdwVtsException;

// Simulate a POST request
$_POST['licenseplate'] = '123XYZ';
$_POST['mileage'] = 10000;

$licenseplate = strtoupper($_POST['licenseplate'] ?? '');
$mileage = $_POST['mileage'] ?? 1;

$url = 'https://services.rdc.nl/voertuigscan/3.0/wsdl';
$identifier = 'your_number';      // Replace with actual RDC customer number
$username = 'your_username';      // Replace with actual username
$password = 'your_password';      // Replace with actual password

$rdcApiService = new RdcVtsApiClient($url, $identifier, $username, $password);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $response = $rdcApiService->getVehicleScan($licenseplate, $mileage);
        echo $response;
    } catch (RdwVtsException $e) {
        echo json_encode(['error' => true, 'message' => $e->getMessage()]);
    }
} else {
    echo json_encode(['error' => true, 'message' => 'Invalid request method']);
}

```

## Contact
Please feel free to contact us, to add options if possible!
