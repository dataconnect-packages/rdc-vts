<?php
namespace RdcVts;

use SoapClient;
use SoapVar;
use SOAPHeader;
use Exception;

class RdcVtsApiClient
{
    protected $url;
    protected $identifier;
    protected $username;
    protected $password;

    public function __construct($url, $identifier, $username, $password)
    {
        $this->url = $url;
        $this->identifier = $identifier;
        $this->username = $username;
        $this->password = $password;
    }

    public function getVehicleScan($licensePlate, $mileage = 1)
    {
        $client = $this->createSoapClient();
        $securityHeader = $this->createSecurityHeader();

        $client->__setSOAPHeaders([$securityHeader]);

        $params = [
            'VoertuigscanQuery' => [
                'Referentie' => 'Gegevens',
                'RDCNummer' => $this->identifier,
                'Kenteken' => $licensePlate,
                'Gegevens' => [
                    'Chassisnummergegevens' => '',
                    'Voertuiggegevens' => '',
                    'Registratiegegevens' => '',
                    'BrandstofEnKleur' => '',
                    'Technischespecificatie' => '',
                    'Milieugegevens' => '',
                    'KentekenNieuwwaarde' => '',
                    'Voertuigstatussen' => '',
                    'Taxatie' => [
                        'Kilometerstand' => $mileage
                    ],
                ]
            ]
        ];

        try {
            $result = $client->opvragenVoertuigscan($params);
            return $this->processResponse($result);
        } catch (Exception $e) {
            return json_encode(['error' => true, 'message' => 'Het kenteken voldoet niet aan de 6 tekens']);
        }
    }

    protected function createSoapClient()
    {
        return new class($this->url, ['trace' => 1]) extends SoapClient {
            protected function callCurl($url, $data)
            {
                $handle = curl_init();
                curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($handle, CURLOPT_URL, $url);
                curl_setopt($handle, CURLOPT_HTTPHEADER, ["Content-Type: text/xml; charset=utf-8", 'SOAPAction: "voertuigscan"']);
                curl_setopt($handle, CURLOPT_ENCODING, "UTF-8");
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
                $response = curl_exec($handle);

                if (empty($response)) {
                    throw new Exception('CURL error: ' . curl_error($handle), curl_errno($handle));
                }

                curl_close($handle);
                return $response;
            }

            public function __doRequest($request, $location, $action, $version, $oneWay = 0): ?string
            {
                return $this->callCurl($location, $request);
            }
        };
    }

    protected function createSecurityHeader()
    {
        $securityXml = '
            <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                           xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                <wsse:UsernameToken>
                    <wsse:Username>' . $this->username . '</wsse:Username>
                    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' . $this->password . '</wsse:Password>
                </wsse:UsernameToken>
            </wsse:Security>';

        return new SOAPHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", 'Security', new SoapVar($securityXml, XSD_ANYXML), true);
    }

    protected function processResponse($result)
    {
        if ($result->Berichtstatus->ServiceStatus == 'Waarschuwing' && $result->Berichtstatus->Foutlijst->Foutcode != 1001) {
            return json_encode(['error' => true, 'message' => $result->Berichtstatus->Foutlijst->Foutmelding ?? 'Onbekende fout']);
        }
    
        $chasisnummerstatus = $result->Voertuigscan->Kentekengegevens->Chassisnummergegevens->Verwerkingsstatus ?? null;
        
        $chassisnummer = ($chasisnummerstatus != '05') 
            ? ($result->Voertuigscan->Kentekengegevens->Chassisnummergegevens->Chassisnummer->_ ?? '')
            : '';
    
        $meldcode = ($chasisnummerstatus != '05') 
            ? ($result->Voertuigscan->Kentekengegevens->Chassisnummergegevens->Meldcode->_ ?? '')
            : '';
    
        $vehicleData = [
            'vin' => $chassisnummer,
            'vinShort' => $meldcode,
            'brand' => $result->Voertuigscan->Typegegevens->Merk ?? '',
            'model' => $result->Voertuigscan->Typegegevens->Model->_ ?? '',
            'variant' => $result->Voertuigscan->Kentekengegevens->TypebeschrijvingVoertuig ?? '',
            'vehicleType' => $result->Voertuigscan->Kentekengegevens->Voertuiggegevens->VoertuigClassificatieRDW->_ ?? '',
            'firstRegistration' => $result->Voertuigscan->Kentekengegevens->Registratiegegevens->DatumEersteInschrijving ?? '',
            'firstAdmission' => $result->Voertuigscan->Kentekengegevens->Registratiegegevens->DatumEersteToelating ?? '',
            'dateOwnership' => $result->Voertuigscan->Kentekengegevens->Registratiegegevens->DatumTenaamstelling ?? '',
            'apkExpiration' => $result->Voertuigscan->Kentekengegevens->Registratiegegevens->APKVervaldatum ?? '',
            'yearBuilt' => $result->Voertuigscan->Kentekengegevens->Registratiegegevens->Bouwjaar->_ ?? '',
            'transmission' => $result->Voertuigscan->Typegegevens->TechnischeSpecificatie->Schakeling->_ ?? '',
            'power' => $result->Voertuigscan->Typegegevens->TechnischeSpecificatie->VermogenPK->_ ?? 0,
            'engineCapacity' => $result->Voertuigscan->Typegegevens->TechnischeSpecificatie->Cilinderinhoud->_ ?? 0,
            'fuel' => $result->Voertuigscan->Kentekengegevens->BrandstofEnKleur->Brandstof->_ ?? '',
            'co2' => $result->Voertuigscan->Kentekengegevens->Milieugegevens->CO2Gecombineerd->_ ?? '',
            'color' => $result->Voertuigscan->Kentekengegevens->BrandstofEnKleur->Kleur->_ ?? '',
            'owners' => $result->Voertuigscan->Kentekengegevens->Registratiegegevens->VolgnummerEigenaar ?? 0,
            'stolenStatus' => $result->Voertuigscan->Kentekengegevens->Voertuigstatussen->StatusGestolen->_ ?? '',
            'WOK' => $result->Voertuigscan->Kentekengegevens->Voertuigstatussen->StatusKeuring->_ ?? '',
            'bpmAmount' => $result->Voertuigscan->Kentekengegevens->Voertuiggegevens->BPMBedragKentekenbewijs ?? 0,
            'bpmRemaining' => $result->Voertuigscan->Taxatiegegevens->Verkoopwaarde->BPM ?? 0,
            'newValue' => $result->Voertuigscan->Kentekengegevens->KentekenNieuwwaarde->PrijsConsumentInBtw->_ ?? 0,
            'commercialValue' => $result->Voertuigscan->Taxatiegegevens->Handelswaarde->Restwaarde->_ ?? 0,
            'tradeinValue' => $result->Voertuigscan->Taxatiegegevens->Inruilwaarde->Restwaarde->_ ?? 0,
            'salesValue' => $result->Voertuigscan->Taxatiegegevens->Verkoopwaarde->Restwaarde->_ ?? 0
        ];
    
        return json_encode(['error' => false] + $vehicleData);
    }
    
}
?>
