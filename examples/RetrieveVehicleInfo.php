<?php
require __DIR__ . '/vendor/autoload.php'; // custom path name to autoload

use RdcVts\RdcVtsApiClient;
use RdcVts\RdcVtsException;

// Simulate a POST request
$_POST['licenseplate'] = '123XYZ';
$_POST['mileage'] = 10000;

$licenseplate = strtoupper($_POST['licenseplate'] ?? '');
$mileage = $_POST['mileage'] ?? 1;

$url = 'https://services.rdc.nl/voertuigscan/3.0/wsdl';
$identifier = 'your_number';      // Replace with actual RDC customer number
$username = 'your_username';      // Replace with actual username
$password = 'your_password';      // Replace with actual password

$rdcApiService = new RdcVtsApiClient($url, $identifier, $username, $password);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $response = $rdcApiService->getVehicleScan($licenseplate, $mileage);
        echo $response;
    } catch (RdwVtsException $e) {
        echo json_encode(['error' => true, 'message' => $e->getMessage()]);
    }
} else {
    echo json_encode(['error' => true, 'message' => 'Invalid request method']);
}
?>